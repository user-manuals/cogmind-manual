\chapter*{Difficulty}

The first time Cogmind starts up, you're presented with a pre-game menu for difficulty selection. Your chosen setting can later be modified in the options menu, although the effect won't kick in until a new run is started.

Cogmind is technically designed for Rogue mode, carefully balanced to provide a fun yet challenging roguelike experience which can be reliably overcome given sufficient experience and skill, but it will put your strategic and tactical analysis skills to the test. It is extremely hard, especially for new players.

Naturally some players simply don't have the time or inclination to strive for mastery, thus alternative modes are available that tweak multiple aspects of the game to make survival easier.

That said, keep in mind that Cogmind relies on a fairly tight design to begin with, and giving you the upper hand via easier settings somewhat destabilizes that design, resulting in a less consistent difficulty curve. Some areas might be significantly easier, almost trivialized, while others remain relatively difficult due to the nature of the world and its mechanics. Also note that even within Rogue mode the world contains places to go and things to do which make the game easier, or even more difficult, if that's your thing!

So that it's more apparent from screenshots which difficulty setting a given player is using, the parts list slot type divider lines in the HUD appear in a different color for each. The stat screen shown at the end of a run also uses different highlight colors.

Important: The same world seed will produce different results in each difficulty setting!

\section*{Rogue}

Rogue mode is the original intended way to play Cogmind, but it's definitely not for everyone. Although every run is winnable, it can take many dozens of hours to reach that level of skill due to the numerous local and global factors working against you, as well as time invested in exploring the wide variety of mechanics and systems you can take advantage of.

This mode reflects the difficulty of a typical traditional roguelike, hard to master but very satisfying as you achieve new milestones, and especially once you win.

\begin{itemize}
	\item Permadeath is enforced (no optional quicksave/load feature)
	\item There are no adjustments for this mode, as it is the standard against which other modes are gauged
\end{itemize}

\section*{Adventurer}

Adventurer mode is still fairly challenging, with a range of minor buffs and mechanical adjustments taking the edge off the unforgiving Rogue mode. Getting far will still take experience and persistence, but it's not quite as intense. Adventurers are Rogues in training. Adjustments:

\begin{itemize}
	\item Quicksave/load support
	\item 20\% base resistance to all damage types
	\item +20\% base accuracy (both ranged and melee)
	\item Enemies alerting nearby allies reveal those allies' positions
	\item Nearby garrisons responding to call for help are revealed on map
	\item Effect of allies on alert level halved
	\item Each map containing Heavies converts 1 of them to a Sentry
	\item Heavy class Active Sensor Suites are 25\% less sensitive
	\item Cargo convoys have 1 less ARC escort
	\item Part corruption effect reduced by 25\%
	\item Disabling machines has less of an impact on alert level
	\item Traveling to a new area/map lowers alert level more than usual
	\item Alert level decay from passing turns doesn't have diminishing returns
	\item +10\% to all direct and indirect hacks at interactive machines
	\item Minimum of 2 indirect database hacks before potential floorwide lockdown
	\item No Operators spawn at depth -10
	\item No Operators spawn within view of starting location below depth -8
	\item +25\% to all Relay Coupler value
	\item Z-hack mapwide cutoff threshold higher
	\item $\lbrack$RPGLIKE mode$\rbrack$ Start with 1,000 extra XP
	\item $\lbrack$RPGLIKE mode$\rbrack$ Gain XP more quickly if fell behind the level curve
	\item $\lbrack$RPGLIKE mode$\rbrack$ Protomatter has somewhat higher value on later maps
	\item $\lbrack$Pay2Buy mode$\rbrack$ Start with 5,000 extra CogCoins
	\item $\lbrack$Polymind mode$\rbrack$ Minimum amount of Protomatter dropped increased by 20\%
	\item $\lbrack$Polymind mode$\rbrack$ Suspicious 0b10 robots raise suspicion 25\% more slowly
\end{itemize} 

\section*{Explorer}

Explorer mode is primarily for those interested in discovering more of the story and exploring various areas of the world that are otherwise much more difficult to reach in other modes. Or just running around shooting things.

Explorer mode is quite easy for those familiar with Cogmind mechanics, certainly too easy by roguelike standards. Yes you can still lose, and certain parts of the world are still going to be pretty dangerous, though with just a little experience it will be easier to avoid them (if you want to) because you'll automatically know where all exits lead. Adjustments:

\begin{itemize}
    \item Quicksave/load support
    \item 35\% base resistance to all damage types
    \item +30\% base accuracy (both ranged and melee)
    \item Enemies alerting nearby allies reveal those allies' positions
    \item Nearby garrisons responding to call for help are revealed on map
    \item Effect of allies on alert level cut to one-third
    \item Disabling machines has a much lower impact on alert level
    \item Traveling to a new area/map lowers alert level much more than usual
    \item Alert level decay from passing turns doesn't have diminishing returns
    \item Alert level decays 50\% faster as turns pass
    \item Each map containing Heavies converts 2 of them to Sentries
    \item Heavy class Active Sensor Suites are 50\% less sensitive
    \item Heavy class sensor-based reinforcements limited to one active squad at a time
    \item Lower chance of random hostile encounters (most notably in mines/caves)
    \item Hostile branch encounters (e.g. in caves/mines) cannot appear directly in spawn area
    \item -1 to all patrol squad sizes (stacks with garrison effect)
    \item -1 to number of garrisons per floor (cannot reduce to 0, however)
    \item Fabricators do not automatically trace and investigate BUILD commands hacked without an Authchip
    \item Cargo convoys have 1 less ARC escort
    \item Always know active cargo convoy route
    \item Exit destinations automatically revealed on sight, or when found via Terrain Scanners
    \item Robot salvage in caves does not self-destruct
    \item Part corruption effect reduced by 50%
    \item Evolve 1 extra slot on exiting junkyard
    \item Junkyard contains random useful utilities
    \item Junkyard Storage Unit is Med. rather than Sml.
    \item Items nearby your spawn in main complex maps replaced by random items more likely to be useful given your current state
    \item No dormant Specialists stationed inside DSFs
    \item +20\% to all direct and indirect hacks at interactive machines
    \item Unlimited free guaranteed indirect database hacks before potential floorwide lockout
    \item No central database lockdowns for indirect record hacking
    \item All direct Record hacks at terminals have an automatic 100\% chance to succeed
    \item No Operators spawn at depth -10
    \item No Operators spawn within view of starting location below depth -7
    \item No Alarm Traps below depth -7
    \item +50\% to all Relay Coupler value
    \item Z-hack mapwide cutoff threshold significantly higher
    \item $\lbrack$RPGLIKE mode$\rbrack$ Start with 3,000 extra XP
    \item $\lbrack$RPGLIKE mode$\rbrack$ Gain XP more quickly if fell behind the level curve
    \item $\lbrack$RPGLIKE mode$\rbrack$ Protomatter has higher value on later maps
    \item $\lbrack$Pay2Buy mode$\rbrack$ Start with 10,000 extra CogCoins
    \item $\lbrack$Polymind mode$\rbrack$ Minimum amount of Protomatter dropped increased by 50\%
    \item $\lbrack$Polymind mode$\rbrack$ Frequency of Protomatter drop preference increased by 25\%
    \item $\lbrack$Polymind mode$\rbrack$ Suspicious 0b10 robots raise suspicion at half normal rate
\end{itemize}

\section*{Saving/Loading}

Cogmind normally saves your progress when you exit the game with an unfinished run, then automatically loads back to that point when starting up again. However, both Adventurer and Explorer modes allow you to also create a separate save point of your own, and load it whenever you want. Access this feature via buttons in the game menu, or use Ctrl-F8/F9 from the main game interface.

Note there is only one slot, so setting a new manual save point will overwrite the previous one. Also this system operates outside Cogmind's regular automatic save/load system, so if you save and exit the game while a run is in progress, that latter save will be the one which is automatically loaded when you return, but you can still always access your same earlier manual save point as long as that same run is in progress.

If no manual saves have been made so far during the current run, a save point will automatically be created at the beginning of each new map, so there is always the option to revert to that point on the current map at any time, or on death. Setting a manual save point will deactivate this feature, lest it override your manually designated save point.

Using the manual save slot is entirely optional, and runs during which this feature is used are not included for stat uploading or leaderboard purposes. Those who want to play Adventurer/Explorer while avoiding the temptation to use this feature can disable it by setting noManualSaving in /user/advanced.cfg.

\section*{Other Options}

You can also increase or at least modify the difficulty and content of the experience by activating either challenge modes or special modes, which are described in the later Alternative Rules section. Some special modes in particular make drastic changes to the way the game plays, which might be more your thing.
