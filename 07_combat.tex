\chapter*{Combat}

\section*{Volley}

When attacking, all active weapons in range of the target are fired at once. Firing a single weapon takes twice as long as a standard action, and subsequent weapons fired in the same volley will take less and less extra time, hence firing more weapons at once is always more time efficient. See the HUD's volley analysis readout to check the actual time required to fire the currently active weapons (which may be further modified by individual weapon delays), as well as the total resource cost of the volley.

Although the full resource cost of the intended volley must be available before firing, energy and matter costs for each weapon are not deducted until that weapon actually fires. All weapons immediately begin generating heat as soon as the volley begins, but for volleys that will require two or more turns, the heat produced is averaged over the total number of turns required to fire.

\section*{Targeting}

Targeting is actually tested on a finer grid than is visible onscreen. Each map space is divided into a 9x9 grid of squares, and as robot sizes vary (S/M/L), they may take up more or fewer of these squares. This has several implications, e.g. you may be able to hit a larger target before it has completely rounded a corner, while smaller targets may require a more direct line-of-sight. It also means that smaller targets are actually easier to shoot around than larger ones. As long as the targeting line is green Cogmind has a clear LOF to the target, even if it looks like it passes through another robot or obstacle.

\section*{Hit Chance}

Many factors affect the chance to hit a target. The chance shown in the scan readout takes into account all factors except those which affect individual weapons in the volley. Per-weapon chances are instead shown in the parts list next to their respective weapons once firing mode is activated and the cursor is highlighting a target.

Base hit chance before any modification is 60\%.
\newline \newline
Volley Modifiers:
\begin{itemize}
	\item +3\%/cell if range < 6
	\item +20\%/30\% if attacker in standard/high siege mode (non-melee only)
	\item +attacker utility bonuses
	\item +10\% if attacker didn't move for the last 2 actions
	\item +3\% of defender heat (if heat positive)
	\item +10\%/+30\% for large/huge targets
	\item +10\% if defender immobile
	\item +5\% w/robot analysis data
	\item -1~15\% if defender moved last action, where faster = harder to hit
	\item -10\% if attacker moved last action (ignored in melee combat)
	\item -3\% of attacker heat (if heat positive)
	\item -10\%/-30\% for small/tiny targets
	\item -10\%/-5\% if target is flying/hovering (and not overweight or in stasis)
	\item -5~15\% if defender running on legs (not overweight) \\ (5\% evasion for each level of momentum)
	\item -5~15\% if attacker running on legs (ranged attacks only) \\ (5\% for each level of momentum)
	\item -20\% for each robot obstructing line of fire
	\item -5\% against Cogmind by robots for which have analysis data
	\item -defender utility bonuses
\end{itemize}

Weapon-specific Modifiers:
\begin{itemize}
	\item +utility bonuses
	\item -recoil (from other weapons)
\end{itemize}

After all modifiers are applied, regardless of the final value hit chance is capped at 95\% (or 100\% for melee attacks). Similarly, there is always at least a 10\% chance to hit a target. The cap is not applied to the per-weapon chances shown in the parts list in order to let you know how much surplus hit chance is available.

Sometimes the log will claim you ``missed'' a target, then destroyed it. That's because you got lucky and the random miss trajectory still hit the target! This is more likely to happen with closer, larger targets as per the mechanics described in the targeting section above.

Note that anything caught in an explosion radius is always hit--explosions automatically damage everything in each space they traverse, be it walls, items, or robots.
\newline\newline
Volley modifiers reordered by type for comparative reference:
\begin{itemize}
	\item +3\%/cell if range < 6
	\item +10\%/+30\% for large/huge targets
	\item -10\%/-30\% for small/tiny targets
	\item +3\% of defender heat (if heat positive)
	\item -3\% of attacker heat (if heat positive)
	\item +20\%/30\% if attacker in standard/high siege mode (non-melee only)
	\item +attacker utility bonuses
	\item -defender utility bonuses
	\item +10\% if defender immobile
	\item +10\% if attacker didn't move for the last 2 actions
	\item -1~15\% if defender moved last action, where faster = harder to hit
	\item -5~15\% if defender running on legs (not overweight) \\ (5\% evasion for each level of momentum)
	\item -10\% if attacker moved last action (ignored in melee combat)
	\item -5~15\% if attacker running on legs (ranged attacks only) \\ (5\% for each level of momentum)
	\item -10\%/-5\% if target is flying/hovering (and not overweight or in stasis)
	\item -20\% for each robot obstructing line of fire
	\item +5\% w/robot analysis data
	\item -5\% against Cogmind by robots for which have analysis data
\end{itemize}

\section*{Guided Weapons}

Guided weapon behavior and operation are different from other weapons. Their projectiles follow a set path designated by a number of waypoints, and will always hit their target, but may not be fired as part of a volley. While a guided weapon is active, only that weapon will fire. Left-click or press Enter to set each waypoint, and use right-clicks or Escape to cancel a waypoint. When ready to fire, put the cursor over the final target and press 'f'. Players preferring pure mouse control and attempting to target an empty space or space out of view can also click on the last waypoint to fire, or assign all waypoints up to the weapon's limit then click on a final target direction.

\section*{Coverage/Exposure}

Each part has a ``coverage'' rating which determines its likeliness to be hit by an incoming attack. Values are relative, so attacks are weighted towards hitting parts with higher coverage. Robot cores also have their own ``exposure'' rating which determines their likeliness to be hit; this value is considered along with part coverage when determining whether an attack will strike the core. The exact chance of a core/part to be hit is shown in parenthesis after its exposure/coverage on the relevant info screen. You can also press 'c' to have the main HUD's parts list display a visualization of relative coverage, i.e. longer bars represent a greater chance for a given part to be hit.
\newline
Some examples of how coverage determines hit locations will help understand how that stat actually works.
\newline\newline
Example 1: Cogmind's core has an exposure of 100. Say you equip only one part, a weapon which also has a coverage of 100. Their total value is 200 (100+100), so if you are hit by a projectile, each one has a 50\% (100/200) chance to be hit.
\newline\newline
Example 2: You have the following parts attached:\newline
Ion Engine (60)\newline
Light Treads (120)\newline
Light Treads (120)\newline
Medium Laser (60)\newline
Light Assault Rifle (100)\newline
With your core (100), the total is 560, so the chance to hit each location is:\newline
Ion Engine: 60/560=10.7\%\newline
Light Treads: 120/560=21.4\% (each)\newline
Medium Laser: 60/560=10.7\%\newline
Light Assault Rifle: 100/560=17.9\%\newline
Core: 100/560=17.9\%\newline

Enemy robots work the same way, so the more parts you blow off, the more likely you are to hit and destroy their core. Armor plating has a very high coverage, so it's more likely to be hit, while tiny utilities such as embedded processors have very low coverage, so you can expect them to last much longer (unless you have little or nothing else covering you). As you progress, your core will become more and more protected by attached parts, because you'll have many more of them, but by that time there are other dangers such as system corruption.

\section*{Damage Overflow}

When a part is destroyed by damage that exceeds its remaining integrity, the surplus damage is then applied directly to the core or another part as chosen by standard coverage/exposure rules. No additional defenses are applied against that damage. Exceptions: There is no damage overflow if the destroyed part itself is armor, and overflow damage always targets armor first if there is any. Critical strikes that outright destroy a part can still cause overflow if their original damage amount exceeded the part's integrity anyway. Damage overflow is caused by all weapons except those of the ``gun'' type, and can overflow through multiple destroyed parts if there is sufficient damage.

\section*{Secondary Targeting}

If a volley including only ``gun''-type weapons destroys a target robot, any and all remaining guns in that volley will lock onto another target, prioritizing the most easily hit armed and active hostile currently in range. This mechanic is also called ``gunslinging.''

\section*{Salvage}

How much of a robot remains to salvage when it is destroyed depends on the value of its cumulative ``salvage modifier'' which reflects everything that happened to it before that point. This internal value is initially set to zero, and each projectile that impacts the robot will contribute its own weapon-based salvage modifier to the total. Some weapons lower the value (most notably ballistic cannons), others have no meaningful effect on it (most guns), while certain types may even raise it, ultimately increasing the likelihood of retrieving useful salvage.

When a robot is destroyed, it leaves an amount of matter equivalent to its salvage potential (usually a random range you can see on its info page), modified directly by the salvage modifier. This means a large enough negative salvage modifier, e.g. from explosives or repeated cannon hits, has the potential to reduce the amount of salvageable matter to zero. A positive salvage modifier can never increase the resulting matter by more than the upper limit of a robot's salvage potential.

The chance for the robot's parts to survive, checked individually for each part, is \\([percent\_remaining\_integrity / 2] + [salvage\_modifier]), thus more damaged parts are more likely to be destroyed completely along with the robot. But even if that check succeeds, there are still two more possible factors that may prevent a given part from dropping. If the robot has any residual heat, parts can be melted, the chance of which is ([heat - max\_integrity] / 4), so again less likely to affect large parts (though still possible, especially at very high heat levels). If the robot was corrupted, parts can be "fried," the chance of which is [system\_corruption - max\_integrity]; by the numbers, this will generally only affect small electronic components like processors, and sometimes devices. Each salvageable part left by a corrupted robot also has a corruption\% chance to itself be corrupted, specifically by a random amount from 1 to (10*[corruption]/100), and when attached will increase Cogmind's system corruption by the same value as well as possibly cause another side effect.

Note that robots built by a fabricator do not leave salvageable parts. Also, anything in a robot's inventory (not attached) is always dropped to the ground, regardless of salvage modifiers or other factors.

\section*{Spotting}

Even when you can see a hostile robot, they will not always notice you immediately. They can only register a target on their own turn, so if you are moving very quickly you can potentially pass into and beyond their field of view before they even have time to react. Sometimes when quickly rounding a corner and spotting an enemy, there is still time to duck back without being seen. That said, if you pass in and out of a hostile robot's field of view before they react, they may still take some notice and decide to investigate whatever they think they saw. Different AIs have a different chance to take notice, with those robots designed for surveillance, for example, being much more likely to investigate a potential passing target.

Another possibility to be aware of: while your own sight range is normally similar to that of other robots (approximately 16 spaces), if you enhance your visual sensors you can spot robots from afar before entering their field of vision. Likewise, hostiles with their own sensors may spot you before you can see them.

To know whether a robot has actually spotted you or not, there are two useful indicators. The first, directly on the map itself, is the sudden glow of their background when they spot you for the first time (also may display as a flashing '!' depending on other UI settings). After that initial sighting you can still refer to the scan window, which displays a red exclamation mark for any hostile robots aware of your presence.

Note that sight range is also reduced when line of sight passes through machines, making it possible in certain environments to get closer to hostiles before being spotted. Cloaking utilities also reduce the range at which enemies can spot you.

\section*{Attack Resolution}

With so many mechanics playing into an attack, especially later in the game when there are numerous active parts involved at once, min-maxers looking to optimize their build may want a clearer understanding of the order in which all elements of an attack are carried out. This manual already covered hit chances above, but there are many more steps to what happens once an attack hits a robot. Below is an ordered list detailing that entire process, which applies to both ranged and melee combat. Note that you most likely DO NOT need to know this stuff, but it may help answer a few specific questions min-maxers have about prioritization.

\begin{enumerate}
	\item Check if the attack is a non-damaging special case such as Datajacks, Stasis Beams, Tearclaws, etc., and handle that before quitting early.
	\item Calculate base damage, a random value selected from the weapon's damage range and multiplied by applicable modifiers for overloading, momentum, and melee sneak attacks, in that order. Potential damage range modified by Melee Analysis Suites, Kinecellerators, and Force Boosters here.
	\item Apply robot analysis damage modifier, if applicable (+10\%).
	\item Apply link\_complan hack damage modifier, if applicable (+25\%).
	\item Apply Particle Charger damage modifier, if applicable.
	\item Reduce damage by resistances.
	\item Apply salvage modifiers from the weapon and any Salvage Targeting Computers.
	\item Determine whether the attack caused a critical hit.
	\item Split damage into a random number of chunks if an explosion, usually 1~3. The process from here is repeated for each chunk.
	\item Store the current damage value as [originalDamage] for later.
	\item Apply the first and only first defense applicable from the following list: phase wall, 75\% personal shield (VFP etc), Force Field, Shield Generator, stasis bubble, active Stasis Trap, Remote Shield, 50\% remote shield (Energy Mantle etc.), Hardlight Generator.
	\item Store the current damage value as [damageForHeatTransfer] for later.
	\item Choose target part (or core) based on coverage, where an Armor Analysis Suite first applies its chance to bypass all armor, if applicable, then available Core Analyzers increase core exposure, before finally testing individual target chances normally.
	\item Cancel critical strike intent if that target has applicable part shielding, or if not applicable to target.
	\item If targeting core, apply damage and, if robot survives, check for core disruption if applicable.
	\item If targeting a power source with an EM weapon, check for a chain reaction due to spectrum.
	\item If not a core hit or chain reaction, prepare to apply damage to target part. If the part is Phase Armor or have an active Phase Redirector, first reduce the damage by their effect(s) and store the amount of reduction as [transferredCoreDamage] for later, then if part is Powered Armor reduce damage in exchange for energy (if available), or if part is treads currently in siege mode reduce damage appropriately. If part not destroyed then also check for EM disruption if applicable.
	\item If part destroyed by damage, record any excess damage as overflow. If outright destroyed by a critical hit but damage exceeded part's integrity anyway, any excess damage is still recorded as overflow.
	\item If part was not armor and the attack was via cannon, launcher, or melee, transfer remaining damage to another random target (forcing to a random armor if any exists). Continue transferring through additional parts if destroyed (and didn't hit armor).
	\item If part not destroyed, check whether heat transfer melts it instead.
    \item Apply [transferredCoreDamage] directly to the core if applicable, with no further modifiers.
	\item Apply damage type side effects:
	\begin{itemize}
		\item Thermal weapons attempt to transfer ([damageForHeatTransfer] / [originalDamage])\% of the maximum heat transfer rating, and also check for possible robot meltdown (the effect of which might be delayed until the robot's next turn).
		\item Kinetic damage may cause knockback.
		\item The amount of EM corruption is based on [originalDamage].
		\item Impact damage may cause knockback, and applies corruption for each part destroyed.
	\end{itemize}
\end{enumerate}

\chapter*{Melee Combat}

Melee weapons can only hit adjacent targets, and can only be used one at a time. Activate a melee weapon and bump (move) into a target to attack it. Other options are to left-click on the target itself, or in that target's general direction. You can also force a melee attack, generally only necessary to use a melee weapon on terrain or a machine, by holding Ctrl-Shift while moving in a certain direction. (Note to vi-key users: Forcing a melee attack this way using vi-keys is not possible because the commands overlap with part swapping, use 'f' to enter targeting mode instead.)

\section*{Hit Chance}

Melee attacks use the same hit chance calculations as ranged combat, with a few exceptions:
\begin{itemize}
	\item Base hit\% is 70
	\item No range modifier
	\item No heat modifiers
	\item Utility modifiers use those applicable to melee combat
\end{itemize}

\section*{Momentum}
All melee weapons benefit from greater momentum in the direction of the attack. Moving multiple consecutive spaces in a row prior to an attack adds additional momentum, up to a maximum of 3. Current momentum is displayed as a number at the end of the HUD's movement data readout.

The damage bonus on a successful melee hit is +1~40\% for non-piercing weapons, or +2~80\% for piercing weapons, calculated as: ([momentum] * [speed\%] / 1200) * 40). For piercing attacks the multiplier is 80 instead of 40. Speed\% is current speed as a percentage of average speed, 100. Thus the more momentum and speed with which you attack, the greater the damage. Your status page shows the damage bonus taking current momentum into account. Note that while some utilities help increase momentum, the maximum bonus damage for a given weapon type cannot be exceeded.

Stopping, performing a non-movement action, or otherwise moving in a direction that does not match the current momentum resets your momentum to 0, except when moving diagonally in the same general direction (e.g. turning from southeast to south). The latter case instead reduces total momentum by 1. Also note that technically any melee attack can take advantage of previously accumulated momentum, regardless of direction. For example, approaching the southern side of a target from the east and attacking northward while passing below it will apply whatever momentum value is displayed in the HUD.

\section*{Sneak Attacks}
Melee attacking an enemy that has not yet noticed you gives a base hit chance of 120\%, and a +100\% damage bonus which stacks with any momentum bonus. Sneak attacks work on any non-fleeing neutral targets as well, since they don't expect you'll suddenly attack them!

\section*{Multi-Wielding}
Although only one primary melee weapon can be active at a time, other attached but inactive melee weapons have a chance to carry out "follow-up attacks" alongside the primary weapon. In Tactical HUD mode, that chance is shown next to each applicable melee weapon. Follow-up attacks are more likely when the primary weapon is slower than a given backup melee weapon. The chance may also be affected by supporting utilities.

Multiple follow-up attacks by different melee weapons are possible in the same action. Each weapon is checked separately, and once confirmed any follow-up attack has a +10\% to hit the target. Each weapon incurs no additional time cost to attack aside from modifying the total attack time by half of their time delay (whether positive or negative). Momentum bonuses that apply to the primary weapon all apply to follow-up attacks as well. The benefits of all actuators also apply to every follow-up attack. If the target is destroyed by an earlier attack, any remaining follow-up attacks will switch to another target in range if there is one.

Datajacks cannot be involved in follow-up attacks, and these attacks are only applicable against robot targets.

\section*{Ramming}
As a last resort, Cogmind can ram other robots to damage and/or push them out of the way. Damage is a random amount from 0 to (((10 + [mass]) / 5) + 1) * ([speed\%] / 100) * [momentum], where speed\% is current speed as a percentage of average speed (100) and effective momentum is a combination of both Cogmind and the target's momentum. However, the damage per attack is capped at 100 before the roll. Smashing into a robot headed straight for you can deal some serious damage, though there are serious negative consequences to go with this unarmed attack, and half as much damage is inflicted on Cogmind as well. Ramming increases the salvage potential of the target (by 3 for each hit), and also enables the collection of a small random amount of matter per collision.

Ramming with active treads or legs always avoids self-damage and destabilization. Treads have a per-tread chance to instantly crush targets of medium size and below which have no more than 50 remaining core integrity. Crushed robots have their salvage modified by -20. Legs have a 20\% chance per leg to kick the target out of the way. (Not applicable against huge targets.)

The time cost to ram is the greater (slower) of 100 and your current move speed.

It is also possible to ram non-robot targets including walls and machines by using the "force melee attack" command (Ctrl-Shift + Movement Keys, or Ctrl-Shift+LMB) without any active melee weapons. Ramming of this type uses the same damage formula as above, but the results are always maximized, and therefore you can know in advance whether the damage will be sufficient to destroy the target. The expected result will be indicated via the usual collision warning when you attempt to ram. Note that ramming a solid object is much more dangerous than ramming a robot, and Cogmind not only takes the same amount of damage that is inflicted on the target, but also suffers other negative side effects as well. The type of propulsion does not affect the results, beyond the indirect effects of carrying capacity and speed. To repeatedly force melee by simply moving into a valid target (or make such an attack compatible with vi-keys), you can toggle force melee mode via the Special Commands interface (Spacebar) or Shift-Alt-e.

\chapter*{Damage types}
Weapon damage is classified into 7 primary types, each with unique properties and effects. Other less common types may be discovered on your travels.

\section*{Thermal}
Thermal energy weapons generally have a shorter range, but benefit from a more easily predictable damage potential and little or no recoil. Thermal damage also generally transfers heat to the target, and may cause meltdowns in hot enough targets.

\section*{Kinetic}
Ballistic weapons generally have a longer effective range and more destructive critical strikes, but suffer from less predictable damage and high recoil. Some kinetic projectiles, especially hypervelocity variants, are capable of penetrating one or more consecutive targets. Kinetic cannon hits also blast usable matter off target robots and have a damage-equivalent chance to cause knockback, with a (10 - range) * 5 modifier and a +/-10\% per size class (T/S/M/L/H) difference between the target and medium size (targets knocked into another robot may also damage and displace it, see Impact below). The amount of matter created per cannon projectile impact is randomized from between zero up to the absolute value of the weapon's salvage modifier, but only applies for those weapons with a negative salvage modifier below -2.

\section*{Electromagnetic}
Electromagnetic (EM) weapons have less of an impact on integrity, but are capable of corrupting a target's computer systems. Anywhere from 50 to 150\% of damage done is also applied as system corruption. (Cogmind is less susceptible to EM-caused corruption, but still has a damage\% chance to suffer 1 point of system corruption per hit.) EM-based explosions only deal half damage to inactive items lying on the ground, but can also corrupt them.

Some EM weapons cause "disruption", which is a per-shot chance to temporarily disable an active part on impact. If a robot core is struck, there is half this chance the entire robot may be disabled. While disrupted a robot may be rewired by bumping into it (see Hacking Robots).

\section*{Explosive}
While powerful, explosives generally spread damage across each target in the area of effect, dividing damage into separate chunks before affecting a robot, where each chunk selects its own target part (though they may overlap).

Explosions also tend to reduce the amount of salvage remaining after destroying a target, and sometimes cause cave-ins in non-reinforced areas.

\section*{Impact}
Impact melee weapons have a damage-equivalent chance to cause knockback, with a +/-10\% per size class (T/S/M/L/H) difference between attacker and target. Targets knocked into another robot may also damage and displace it. A robot hit by another displaced robot has a chance to itself be displaced and sustain damage, where the chance equals the original knockback chance further modified by +/-10\% per size class difference between the blocking robot and the knocked back robot, and the resulting damage equals [originalDamage] (see Attack Resolution), further divided by the blocker size class if that class is greater than 1 (where Medium = 2, and so on).

Impact weapons ignore relative coverage when determining which components to affect, and are therefore effective at destroying fragile systems regardless of armor and other protection. By ignoring coverage, there is an equal chance for an attack to hit any given part, where each slot of a multislot part also has a separate and equivalent chance to be hit. A target's core also counts as one slot for that purpose. In addition, for every component crushed by an impact, its owner's system is significantly corrupted (+25-150\%), though electromagnetic resistance can help mitigate this effect. (Cogmind is less susceptible to corruption caused in this manner.)

\section*{Slashing}
Slashing melee weapons are generally very damaging, and most are also capable of severing components from a target without destroying them.

\section*{Piercing}
Piercing melee weapons inflict less collateral damage, but are more likely to hit a robot's core, and get double the melee momentum damage bonus.

\chapter*{Heat}
Movement, firing weapons, and running power sources and some utilities generates heat. Some of this heat is naturally dissipated by the core's own heat sinks, but not enough to deal with heat generated by numerous/large energy weapons. Heat sinks and cooling systems can be used to avoid overheating, which can have a wide range of negative effects. Heat is only a significant issue for robots that rely heavily on energy weapons. However, note that when firing a volley the heat produced is averaged over the volley's turn duration rather than being immediately applied all at once.

\section*{Side Effects}
Once heat reaches the "Hot" level (200+), active utilities and weapons may be temporarily disabled. At "Warning" levels (300+) power sources are likely to shut down. Many more serious (and permanent) effects are possible, especially at higher heat levels.

Disabled power sources automatically restart when possible, but other parts must be manually reactivated.

Heat effects are not calculated until after the dissipation phase, so heat can temporarily spike very high with no side effects as long as there are sufficient utilities to dissipate it.