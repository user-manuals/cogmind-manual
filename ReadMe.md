### About this repository.

This repository contains:

- pregenerated game manual in the PDF format,
- source code in the tex format,
- all necessary graphics,
- licenses of fonts used,
- original manual.txt, copied from the latest stable game release.

To compile this manual properly, you will need to put font files into project root directory. Fonts used:

- Hall Fetica (available on [1001fonts.com](https://www.1001fonts.com/hall-fetica-font.html)),
- Open Sans Regular (available on [google fonts](https://fonts.google.com/specimen/Open+Sans)).

Please note that **this is not an official work.** If you are looking for official Cogmind materials, please visit https://www.gridsagegames.com/cogmind/.

I aim to keep this LaTeX-ed manual in sync with game's releases. The text of the instructions will not be changed, except in special circumstances (eg "modifying this document will change the text in the game" would become "modifying the manual.txt file will change the text in the game").

