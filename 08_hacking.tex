\chapter*{Hacking Machines}

Interactive machines appear in color rather than gray. Access their interface by bumping (moving) into the solid color space containing the representative letter.

\section*{Factors}

Each function/command at a machine has its own base difficulty to hack, though there are numerous other factors at play in determining your chance of success.
\begin{itemize}
	\item Security Level: Each class of machines exists in three different varieties, rated by security level from 1 to 3. Higher security machines provide greater functionality, but are correspondingly more difficult to hack.
	\item Utilities: Active hackware contributes to hacking attempts, most directly by making each hack easier.
	\item System Corruption: Being corrupted decreases the chance of success (-corruption/3\%).
	\item Operators: Having a network of operators under your control helps bypass machine security systems, though the benefits decrease with each additional operator.
\end{itemize}

\section*{Process}

From your point of view, hacking is simply connecting to a system and choosing/entering commands. However, the system itself responds in three phases:
\begin{itemize}
	\item Detection: Initially your presence is unknown, but with each subsequent action there is a chance your activity will be detected. Detection happens more quickly at higher security machines, and becomes more likely with each failed hack, but can be mitigated by defensive hackware. Accessing the same machine more than once also increases the chance of detection, if it was previously hacked but not traced.
	\item Tracing: As soon as suspicious activity is detected, the system attempts to locate it. Failing hacks increases the speed of the trace, more quickly for worse failures. If a session is terminated while a trace is in progress, that trace will resume from where it left off if a connection is reestablished.
	\item Feedback: Once traced, the system is permanently locked and may attempt to counterattack the source of the unauthorized access, which either causes system corruption or disables connected hackware.
	\item Note that successful hacks (especially those which were difficult to pull off) have a chance to cause an increase in local alert level, though this result is less likely while using defensive hackware.
\end{itemize}

\section*{Terminals}

Terminals are the most common type of interactive machine, providing access to local and central networks and databases. At the time of access, the terminal interface lists all potential hacking targets found for direct hacking. Unlisted targets can be hacked by manually entering the associated command, though so-called ``indirect hacking'' is more difficult (-15\% chance per security level, though offensive hackware can be used to counteract this difficulty). The precise difficulty of indirect hacks is also an unknown factor, making them more dangerous for the inexperienced hacker.

Terminal records are purely informational, and will teach you more about the world and its inhabitants. Those you have never hacked before, and therefore do not yet appear in your lore collection, will have a '!' prepended to their target name. This reminder can be deactivated in /user/advanced.cfg via the markUndiscoveredLore option.

Operators are stationed at some terminals. Destroying the operator and retrieving its data core will extract its dynamic key and provide a 1.5x bonus to the first hack at that operator's terminal, but only if the connection is established before the key expires. Nearby trap locations can also be de-encrypted from pre-expiry data cores.

High-security terminals have direct access to more functions at once.

\section*{Fabricators}

Fabricators construct parts and robots based on schematics. Schematics can be acquired by using scanalyzers or hacking terminals, then loaded into a fabricator which will report the resources required to complete construction. Matter is drawn from the local fabrication network.

High-security fabricators are capable of faster construction.

\section*{Repair Stations}

Repairing parts is a two-stage process. First instruct the station to scan a component currently in your inventory. Then initiate the repair process, which both fixes broken parts and restores them to full integrity.

The separate ``refit'' command scans a robot and attempts to restore missing functionality using simple backup parts.

High-security repair stations are capable of faster repairs.

\section*{Recycling Units}

Recycle components by loading them from your inventory and initiating the process. Recycling units collect matter until they reach a certain quota (500), after which it is transferred away to a central system. Instruct the unit to report matter to examine its current stores. Hack the retrieve matter command to have it eject its contents.

Similarly, unprocessed parts can be listed and retrieved via other system commands.

\section*{Scanalyzers}

Scanalyzers provide schematics for existing parts. Insert a part from your inventory, followed by the instruction to analyze. See your status page for access to a list of schematics currently in your possession, and note that in part info visualization mode ('q') data preceded by a '+' indicates you have a schematic for that part.

\Kern{+1.0}{More powerful scanalyzers generally require fewer scans to successfully create a proper schematic, and are less likely to damage the scanned part.}

\section*{Manual Hacking}

Manual entry of hacking commands has multiple uses. At terminals this feature can be used to indirectly hack unlisted targets. The text to enter commands is learned by observing the results window output during a regular listed hack. (For example, enter ``Alert(Check)'' at any terminal to attempt that hack.)

To simplify repeat hacks, all previously entered manual commands are stored in a buffer; once the >> command prompt is active, press Up/Down arrows to cycle through them. (If necessary you can edit this buffer directly, found in /user/buffer.txt, though editing should not be done while the game is running.)

Manual hacking also supports autocompletion. As you type it will compile a list of all matching commands, showing the first as grayed out text. Press tab to accept the gray text rather than typing it out in full. For subcommands, those parts of a command after a left parenthesis, if there are multiple options a list will appear allowing you to optionally press up/down to cycle through them and tab to accept. Continuing to type will filter the list down further. From the outset all common commands are considered for the autocompletion list, while unauthorized hacks you learn on your travels are considered only after you discover them (or if you have entered them directly at least once before).

Successful indirect hacking of central ``database-related'' targets (queries, schematics, analysis, prototypes) incurs a 25\% chance to trigger a database lockout, preventing indirect access to those types of targets at every terminal on the same map. The chance of a lockout is reduced while using defensive hacking utilities.

Specific to manually hacking robot schematics and analyses, note that entering the class name will auto-select the best relevant variant available at the given terminal, from among those you don't already have. (For example, ``Schematic(Grunt)'' tries to hack the best applicable Grunt-type variant.) Manually hacking robot schematics/analyses and loading schematics also allows for case-insensitive partial string matching. For example the string ``troop'' would be automatically expanded to "G-47 Trooper" because that is the first substring match. If more than one match exists, the first one found in the database is always chosen.

Manual hacking can also be used to enter unique codes obtained on your travels. Any such codes learned in the current run will be listed automatically on beginning a manual hack. Non-mouse players can type the code prefix ``$\backslash$$\backslash$'' to cause the focus to switch to that menu in order to select an option by letter (or Escape/Backspace to return to the normal text entry). If the focus is already in the menu, pressing Escape returns to the text area to allow manual entry or editing as necessary.

Manual hacking commands are case-insensitive, including both the command itself and any optional arguments.

\chapter*{Hacking Robots}

Access a robot's system using either a melee or remote datajack. While the parse\_system hack can be used on almost any robot, most other hacks are limited to certain targets. Using a datajack on a target is not considered a hostile action, and targets won't even notice.

\section*{Requirements}

Any hacks listed in the Basic category are accessible with nothing more than a datajack. ``RIF'' category hacks require that you've installed a Relay Interface Framework via the dedicated machine found inside any garrison. Most hacks require having also attached a robot-appropriate Relay Coupler, which can be acquired in a number of ways.

\section*{Cost}

The cost of performing a coupler-based hack is listed next to the hack, and deducted from the coupler's remaining value when performed.

\section*{Manual Hacking}

Manual entry of standard robot hacking commands is not necessary, but offered as an optional method.

To simplify repeat hacks, all previously entered manual commands are stored in a buffer; once the >> command prompt is active, press Up/Down arrows to cycle through them. (If necessary you can edit this buffer directly, found in /user/buffer\_robot.txt, though editing should not be done while the game is running.)

Manual hacking also supports autocompletion. As you type it will compile a list of all matching commands, showing the first as grayed out text, possibly followed by a number indicator if there is more than one match available. Press spacebar or tab to accept the gray text rather than typing it out in full, or press up/down to cycle through other options.

Manual hacking can also be used to enter unique codes obtained on your travels. Any such codes learned in the current run will be listed automatically on beginning a manual hack. Non-mouse players can type the code prefix ``$\backslash$'' to cause the focus to switch to that menu in order to select an option by letter (or Escape/Backspace to return to the normal text entry).

Manual hacking commands are case-insensitive, including both the command itself and any optional arguments.

\section*{Rewiring}

\enlargethispage{\baselineskip}
While a robot is disrupted (usually after taking some forms of EM damage to its core), bump into it to attempt a rewire, which if successful will assimilate it. No additional utilities are required, though the chance of success is improved by offensive hackware. Disrupted allies can be instantly reactivated in the same manner.

The base chance of rewiring success is 10\%, or 50\% with a datajack.

If flying, note that you'll need to switch to some other form of propulsion in order to come down and rewire a target via bumping.
